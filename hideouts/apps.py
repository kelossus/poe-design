from django.apps import AppConfig


class HideoutsConfig(AppConfig):
    name = 'hideouts'
