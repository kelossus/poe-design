# poe-design

A website for users to share their hideouts from pathofexile.com


To build the code and run:
 * docker-compose up -d --build

To shut the server down:
 * docker-compose down

To test the service
 * docker-compose run web python /code/manage.py test

 python manage.py makemigrations posts